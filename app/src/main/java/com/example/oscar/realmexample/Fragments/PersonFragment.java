package com.example.oscar.realmexample.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.oscar.realmexample.MainActivity;
import com.example.oscar.realmexample.Models.Person;
import com.example.oscar.realmexample.Models.Phone;
import com.example.oscar.realmexample.PersonActivity;
import com.example.oscar.realmexample.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonFragment extends Fragment {


    @BindView(R.id.personName)
    EditText personName;
    @BindView(R.id.personAge)
    EditText personAge;
    @BindView(R.id.personHeight)
    EditText personHeight;
    @BindView(R.id.phoneName)
    EditText phoneName;
    @BindView(R.id.phoneYear)
    EditText phoneYear;
    @BindView(R.id.phoneAndroid)
    EditText phoneAndroid;
    @BindView(R.id.phoneUsage)
    CheckBox phoneUsage;
    @BindView(R.id.btnSave)
    Button btnSave;
    Unbinder unbinder;
    String id = "";
    Person personUpdate;
    public PersonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_person2, container, false);
        unbinder = ButterKnife.bind(this, view);


        if(getActivity().getIntent().hasExtra("id")){
            id = getActivity().getIntent().getStringExtra("id");
            Realm realm = Realm.getDefaultInstance();
            personUpdate = realm.where(Person.class).equalTo("uniqueID",id).findFirst();
            setData();
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressWarnings("ConstantConditions")
    void setData(){
        personName.setText(personUpdate.getName());
        personAge.setText(String.valueOf(personUpdate.getAge()));
        personHeight.setText(String.valueOf(personUpdate.getHeight()));
        phoneName.setText(personUpdate.getPhone().first().getName());
        phoneYear.setText(String.valueOf(personUpdate.getPhone().first().getYear()));
        phoneAndroid.setText(String.valueOf(personUpdate.getPhone().first().getAndroidVersion()));
        phoneUsage.setChecked(personUpdate.getPhone().first().getInUse());
    }

    @OnClick(R.id.btnSave)
    public void onViewClicked() {
        boolean flag=false;
        try{
        if(personName.getText().toString().trim().isEmpty()){
            flag =true;
        }
        if(personAge.getText().toString().trim().isEmpty()){
            flag=true;
        }
        if(personHeight.getText().toString().trim().isEmpty()){
            flag=true;
        }

        if(phoneName.getText().toString().trim().isEmpty()){
            flag=true;
        }
        if(phoneAndroid.getText().toString().trim().isEmpty()){
            flag=true;
        }
        if(phoneYear.getText().toString().trim().isEmpty()){
            flag=true;
        }

        if(flag){
            Toast.makeText(getContext(),"Revise los campos",Toast.LENGTH_SHORT).show();
        }else {
            Person person;
            Phone phone;
            if (id.isEmpty()) {
                person = new Person();
                phone = new Phone();
            } else {
                person = personUpdate;
                phone = personUpdate.getPhone().first();
            }

            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            person.setName(personName.getText().toString());
            person.setAge(Integer.parseInt(personAge.getText().toString()));
            person.setHeight(Double.parseDouble(personHeight.getText().toString()));

            phone.setName(phoneName.getText().toString());
            phone.setYear(Integer.parseInt(phoneYear.getText().toString()));
            phone.setAndroidVersion(Double.parseDouble(phoneAndroid.getText().toString()));
            phone.setInUse(phoneUsage.isChecked());

            RealmList<Phone> realmList = new RealmList<>();
            realmList.add(phone);

            person.setPhone(realmList);

            realm.copyToRealmOrUpdate(person);
            realm.commitTransaction();
            realm.close();

            Toast.makeText(getContext(), "Guardado con exito", Toast.LENGTH_SHORT).show();

            getActivity().onBackPressed();
        }
        }catch (Exception e){
            Toast.makeText(getContext(), "Vaya, verifica los datos", Toast.LENGTH_SHORT).show();
        }
    }
}
