package com.example.oscar.realmexample.Models;

import io.realm.RealmObject;

/**
 * RealmExample
 * File created by oscar on 21/1/2018.
 * Please keep the coding standards in this file
 */

public class Phone extends RealmObject{

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getInUse() {
        return inUse;
    }

    public void setInUse(Boolean inUse) {
        this.inUse = inUse;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(Double androidVersion) {
        this.androidVersion = androidVersion;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    String name;
    Boolean inUse;
    Integer year;
    Double androidVersion;
    Integer ram;
}
