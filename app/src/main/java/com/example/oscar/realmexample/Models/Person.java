package com.example.oscar.realmexample.Models;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * RealmExample
 * File created by oscar on 21/1/2018.
 * Please keep the coding standards in this file
 */

public class Person extends RealmObject{

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public RealmList<Phone> getPhone() {
        return phone;
    }

    public void setPhone(RealmList<Phone> phone) {
        this.phone = phone;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    @PrimaryKey
    String uniqueID = UUID.randomUUID().toString();
    String name;
    Integer age;
    Double height;
    RealmList<Phone> phone;

}
