package com.example.oscar.realmexample;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.oscar.realmexample.Models.Person;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * RealmExample
 * File created by oscar on 21/1/2018.
 * Please keep the coding standards in this file
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    List<Person> person;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView name;
        TextView age;
        TextView height;
        TextView phone;
        TextView id;
        LinearLayout layout;
        public ViewHolder(CardView v) {
            super(v);
            name = v.findViewById(R.id.name);
            age = v.findViewById(R.id.age);
            height = v.findViewById(R.id.height);
            phone = v.findViewById(R.id.phone);
            id =v.findViewById(R.id.id);
            layout = v.findViewById(R.id.holder);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<Person> person) {
        this.person = person;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        /*
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        */

        CardView v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card,parent,false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.name.setText("Nombre: "+person.get(position).getName());
        holder.age.setText("Edad: "+String.valueOf(person.get(position).getAge()));
        holder.height.setText("Altura: "+String.valueOf(person.get(position).getHeight()));
        holder.phone.setText("Marca telefono: "+person.get(position).getPhone().first().getName());
        holder.id.setText(person.get(position).getUniqueID());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Persona",holder.id.getText().toString());
                Intent intent = new Intent(view.getContext(), PersonActivity.class);
                intent.putExtra("id",holder.id.getText().toString());
                view.getContext().startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return person.size();
    }
}